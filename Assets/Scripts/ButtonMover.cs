﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonMover : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    private Button button;
    private MoveToPoint moveToPoint;
    [SerializeField] private Vector2 startPosition, endPosition;
    private bool canMove;

    public void OnPointerDown(PointerEventData eventData)
    {
        if (canMove)
        {
            canMove = false;
            moveToPoint.Move(endPosition);
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Invoke("CanMoveTrue", moveToPoint.MoveTime);
        moveToPoint.Move(startPosition);
    }

    private void Awake()
    {
        moveToPoint = GetComponent<MoveToPoint>();
        button = GetComponent<Button>();
    }

    private void CanMoveTrue()
    {
        canMove = true;
    }
}
