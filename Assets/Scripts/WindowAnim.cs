﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowAnim : MonoBehaviour
{
    [SerializeField] private iTween.EaseType easetype = iTween.EaseType.easeInOutSine;
    [SerializeField] private float scaleTime;
    void Start()
    {
        Invoke("Show", 0.5f);
    }

    private void Show()
    {
        iTween.ScaleTo(gameObject, iTween.Hash(
            "x", 1,
            "y", 1,
            "z", 1,
            "time", scaleTime,
            "easetype", easetype));
    }

}
