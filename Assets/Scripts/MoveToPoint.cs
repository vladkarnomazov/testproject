﻿using UnityEngine;
using System.Collections;

public class MoveToPoint : MonoBehaviour
{

    private RectTransform rect;
    [SerializeField] private iTween.EaseType easetype = iTween.EaseType.easeInOutSine;
    [SerializeField] private float moveTime;
    public float MoveTime => moveTime;

    private void Awake()
    {
        rect = GetComponent<RectTransform>();
    }
    
    public void Move(Vector2 pointPos)
    {
        iTween.ValueTo(gameObject, iTween.Hash(
            "from", rect.anchoredPosition,
            "to", pointPos,
            "time", moveTime,
            "easetype", easetype,
            "onupdate", "SetPosition"));
    }

    private void SetPosition(Vector2 pos)
    {
        rect.anchoredPosition = pos;
    }

}
