﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderMover : MonoBehaviour
{
    private RectTransform rect;
    [SerializeField] private Vector2 startPosition, endPosition;
    [SerializeField] private Color endColor, startColor;
    private Button swapButton;
    private Image getImage;
    private MoveToPoint moveToPoint;
    private bool canMove = true;

    public void Start()
    {

        Hashtable tweenParams = new Hashtable();
        tweenParams.Add("from", getImage.color);
        tweenParams.Add("to", endColor);
        tweenParams.Add("time", 0.5f);
        tweenParams.Add("onupdate", "OnColorUpdated");

        iTween.ValueTo(getImage.gameObject, tweenParams);
        
    }
    private void OnColorUpdated(Color color)
    {
        getImage.color = color;
    }
    private void Awake()
    {
        getImage = GetComponent<Image>();
        moveToPoint = GetComponent<MoveToPoint>();
        rect = GetComponent<RectTransform>();
        swapButton = GetComponent<Button>();
        swapButton.onClick.AddListener(MoveToPosition);



    }


    private void MoveToPosition()
    {
       
        if (!canMove) return;

        canMove = false;
        if (rect.anchoredPosition.x > 0)
        {
            moveToPoint.Move(startPosition);
            iTween.ValueTo(gameObject, iTween.Hash(
                "from", getImage.color,
                "to", startColor,
                "time", moveToPoint.MoveTime,
                "onupdate", "OnColorUpdated"));

        }
        else
        {
            moveToPoint.Move(endPosition);
            iTween.ValueTo(gameObject, iTween.Hash(
                "from", getImage.color,
                "to", endColor,
                "time", moveToPoint.MoveTime,
                "onupdate", "OnColorUpdated"));

        }
        Invoke("CanMoveTrue", moveToPoint.MoveTime);
    }
    private void CanMoveTrue()
    {
        canMove = true;
    }

    
}
